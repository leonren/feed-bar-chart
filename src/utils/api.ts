import fetch from "node-fetch";

export async function getDataFromAwsApi(): Promise<any> {
  console.log("============> getDataFromAwsApi");
  try {
    const res = await fetch(
      `https://26pfmytm3e.execute-api.ap-southeast-2.amazonaws.com/dev`
    );
    const json = await res.json();
    console.log(json);
    return json;
  } catch (error) {
    console.error("Can not fetch data");
  }
}
