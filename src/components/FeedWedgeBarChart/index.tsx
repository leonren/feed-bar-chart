import React from "react";
import "./FeedWedgeBarChart.scss";
import { cover } from "../../types/CommonTypes";

interface FeedWedgeBarChartProps {
  covers: Array<cover>;
  preGrazing: number;
  postGrazing: number;
  showTarget: boolean;
}

interface FeedWedgeBarChartState {
  histogramContainerWidth: number;
  histogramWidth: number;
  toolTipsShow: Array<boolean>;
}
export default class FeedWedgeBarChart extends React.Component<
  FeedWedgeBarChartProps,
  FeedWedgeBarChartState
> {
  private histogramContainer: any;
  private histogramBarWidth: number;
  private histogramBarPadding: number;
  private histogramBarSpacing: number;

  constructor(props: Readonly<FeedWedgeBarChartProps>) {
    super(props);

    this.state = {
      toolTipsShow: [],
      histogramWidth: 0,
      histogramContainerWidth: 0
    };

    this.histogramContainer = React.createRef();
    this.histogramBarWidth = 24;
    this.histogramBarPadding = 8;
    this.histogramBarSpacing =
      this.histogramBarPadding * 2 + this.histogramBarWidth;
  }

  sliderController = (e: React.FormEvent<HTMLInputElement>) => {
    e.preventDefault();
    const container = this.histogramContainer.current;
    const total = container.scrollWidth - container.offsetWidth;
    const percentage = total * (parseInt(e.currentTarget.value) / 100);
    container.scrollLeft = percentage;
  };

  doRectMouseOver = (e: any) => {
    const rect = e.currentTarget;
    const tempArray = this.state.toolTipsShow;
    tempArray[parseInt(rect.id, 10)] = true;
    this.setState({ toolTipsShow: tempArray });
  };

  doRectMouseOut = (e: any) => {
    const rect = e.currentTarget;
    const tempArray = this.state.toolTipsShow;
    tempArray[parseInt(rect.id, 10)] = false;
    this.setState({ toolTipsShow: tempArray });
  };
  componentDidMount() {
    this.setState({
      histogramWidth: this.props.covers.length * this.histogramBarSpacing + 16,
      histogramContainerWidth: this.histogramContainer.current.offsetWidth
    });
  }

  componentDidUpdate() {}

  sortPaddocks(leftCover: cover, rightCover: cover) {
    return rightCover.cover - leftCover.cover;
  }

  renderYaxis = (): JSX.Element => {
    return (
      <svg
        className="y_axis"
        height="224"
        width="96"
        version="1.1"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 96 224"
      >
        {/* Y Axis */}
        <line className="st0" x1="96" y1="10" x2="96" y2="192" />
        {/* Y Axis scale */}
        <line className="st0" x1="88" y1="32" x2="96" y2="32" />
        <line className="st0" x1="88" y1="64" x2="96" y2="64" />
        <line className="st0" x1="88" y1="96" x2="96" y2="96" />
        <line className="st0" x1="88" y1="128" x2="96" y2="128" />
        <line className="st0" x1="88" y1="160" x2="96" y2="160" />
        <line className="st0" x1="88" y1="192" x2="96" y2="192" />
        {/* Y Axis scale */}

        {/* Y Axis scale value*/}
        <text transform="matrix(1 0 0 1 76.5249 196.6959)" className="st1 st2">
          0
        </text>
        <text transform="matrix(1 0 0 1 52.4108 164.6963)" className="st1 st2">
          1,000
        </text>
        <text transform="matrix(1 0 0 1 50.1496 36.6961)" className="st1 st2">
          5,000
        </text>
        <text transform="matrix(1 0 0 1 50.4479 68.6961)" className="st1 st2">
          4,000
        </text>
        <text transform="matrix(1 0 0 1 50.6427 100.696)" className="st1 st2">
          3,000
        </text>
        <text transform="matrix(1 0 0 1 50.6558 132.6962)" className="st1 st2">
          2,000
        </text>
        {/* Y Axis scale value*/}
        <text
          transform="matrix(6.123234e-17 -1 1 6.123234e-17 26.6456 137.9932)"
          className="st1 st2"
        >
          kg/DM/ha
        </text>
      </svg>
    );
  };

  renderChartDescription(averagePastureCover: number): JSX.Element {
    return (
      <div>
        <h5>Average Pasture Cover: {averagePastureCover} kg/Dm/ha</h5>
        <ul className="fw_legend">
          <li>
            <div className="fw_legend_available fw_legend_icon"></div>
            Cover
          </li>
        </ul>
      </div>
    );
  }

  render() {
    let paddocks = this.props.covers;
    paddocks.sort(this.sortPaddocks);

    const histogramCoverBars: Array<JSX.Element> = [];
    const miniMapCover: Array<JSX.Element> = [];
    const paddockLabels: Array<JSX.Element> = [];
    const toolTips: Array<JSX.Element> = [];

    //Initial Average Pasture Cover
    let averagePastureCoverTally = 0;
    //Initial histogram position value
    let histogramX = this.histogramBarPadding * 2;
    //Initial mini map position value
    const minimapChunks =
      (this.state.histogramContainerWidth - 16) / paddocks.length;
    const minimapSpacing = minimapChunks / 3;
    let minimapX = 10;
    const minimapWidth = minimapSpacing * 2;
    const sliderHandleWidth =
      (this.state.histogramContainerWidth / this.histogramBarSpacing) *
      minimapChunks;

    //Set Start and end point of grazing line
    //192 is the height of Y axis
    const target_start = 192 - this.props.preGrazing / 32;
    const target_end = 192 - this.props.postGrazing / 32;

    for (let i = 0; i < paddocks.length; i++) {
      // main histogram
      averagePastureCoverTally += paddocks[i].cover;
      let available = paddocks[i].cover / 32;
      let availYAxis = 192 - available;
      histogramCoverBars.push(
        <rect
          x={histogramX}
          y={availYAxis}
          className="fw_bar_available"
          width={this.histogramBarWidth}
          height={available}
          key={paddocks[i].name}
          data-name={paddocks[i].name}
          data-cover={paddocks[i].cover}
          onMouseOver={this.doRectMouseOver}
          onMouseOut={this.doRectMouseOut}
          id={i.toString()}
        />
      );

      paddockLabels.push(
        <text
          transform={`translate(${histogramX}, 218)`}
          text-anchor={i === paddocks.length - 1 ? "end" : "start"}
        >
          <tspan>
            {paddocks[i].name.length > 4
              ? `${paddocks[i].name.slice(0, 4)}...`
              : paddocks[i].name}
          </tspan>
        </text>
      );

      toolTips.push(
        <text
          transform={`translate(${histogramX}, ${availYAxis - 15})`}
          text-anchor={paddocks.length - i < 4 ? "end" : "start"}
          display={this.state.toolTipsShow[i] ? "" : "none"}
        >
          <tspan>{`name: ${paddocks[i].name} `}</tspan>
          <tspan>{`cover: ${paddocks[i].cover}`}</tspan>
        </text>
      );
      histogramX += this.histogramBarSpacing;
      // minimap histogram
      available = paddocks[i].cover / 96;
      availYAxis = 40 - available;
      miniMapCover.push(
        <rect
          x={minimapX}
          y={availYAxis}
          className="fw_bar_available"
          width={minimapWidth}
          height={available}
          data-name={paddocks[i].name}
          data-cover={paddocks[i].cover}
          key={paddocks[i].name + "_minimap"}
        />
      );

      minimapX += minimapSpacing + minimapWidth;
    }

    const averagePastureCover = averagePastureCoverTally / paddocks.length;

    let targetLine = null;
    if (this.props.showTarget) {
      targetLine = (
        <line
          className="fw_target"
          x1="4"
          y1={target_start}
          x2={this.state.histogramWidth - 8}
          y2={target_end}
          strokeWidth="2"
        />
      );
    }

    const histogram_viewbox = "0 0 " + this.state.histogramWidth + " 224";

    return (
      <div className="fw_container">
        {this.renderYaxis()}
        <div className="fw_histogram_container" ref={this.histogramContainer}>
          {this.renderChartDescription(averagePastureCover)}
          <svg
            className="y_axis"
            height="224"
            width={this.state.histogramWidth}
            version="1.1"
            xmlns="http://www.w3.org/2000/svg"
            viewBox={histogram_viewbox}
          >
            <line
              className="st0a"
              x1="0"
              y1="32"
              x2={this.state.histogramWidth}
              strokeDasharray="2 2"
              y2="32"
            />
            <line
              className="st0a"
              x1="0"
              y1="64"
              x2={this.state.histogramWidth}
              strokeDasharray="2 2"
              y2="64"
            />
            <line
              className="st0a"
              x1="0"
              y1="96"
              x2={this.state.histogramWidth}
              strokeDasharray="2 2"
              y2="96"
            />
            <line
              className="st0a"
              x1="0"
              y1="128"
              x2={this.state.histogramWidth}
              strokeDasharray="2 2"
              y2="128"
            />
            <line
              className="st0a"
              x1="0"
              y1="160"
              x2={this.state.histogramWidth}
              strokeDasharray="2 2"
              y2="160"
            />

            <line
              className="st0"
              x1={this.state.histogramWidth}
              y1="32"
              x2={this.state.histogramWidth}
              y2="192"
            />

            {histogramCoverBars}
            {targetLine}
            {paddockLabels}
            {toolTips}
            <line
              className="st0"
              x1="0"
              y1="192"
              x2={this.state.histogramWidth}
              y2="192"
            />
          </svg>
          <h6 className="st1 st2">Paddocks</h6>
        </div>
        <div className="fw_minimap">
          <style
            dangerouslySetInnerHTML={{
              __html:
                ".fw_slider::-webkit-slider-thumb { width:" +
                sliderHandleWidth +
                "px;}"
            }}
          ></style>
          <input
            type="range"
            min="0"
            max="100"
            className="fw_slider"
            defaultValue="0"
            onChange={this.sliderController}
          />
          <svg
            className="fw_minimap_view"
            version="1.1"
            xmlns="http://www.w3.org/2000/svg"
          >
            {miniMapCover}
          </svg>
        </div>
      </div>
    );
  }
}
