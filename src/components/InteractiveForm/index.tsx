import React from "react";
import FeedWedgeBarChart from "../FeedWedgeBarChart";
import "./InteractiveForm.scss";
import { data } from "../FeedWedgeBarChart/data";

interface InteractiveFormState {
  growth: number;
  preGrazing: number;
  postGrazing: number;
  showTarget: boolean;
}
export default class InteractiveForm extends React.Component<
  {},
  InteractiveFormState
> {
  constructor(props: Readonly<{}>) {
    super(props);

    this.state = {
      growth: 20,
      preGrazing: 2500,
      postGrazing: 1500,
      showTarget: false
    };
  }

  updatePost = (e: React.FormEvent<HTMLInputElement>): void => {
    this.setState({ postGrazing: parseInt(e.currentTarget.value, 10) });
  };

  updatePre = (e: React.FormEvent<HTMLInputElement>): void => {
    this.setState({ preGrazing: parseInt(e.currentTarget.value, 10) });
  };

  updateShowTarget = (e: React.FormEvent<HTMLInputElement>): void => {
    this.setState({ showTarget: e.currentTarget.checked });
  };

  render() {
    return (
      <div className="content">
        <FeedWedgeBarChart
          covers={data}
          postGrazing={this.state.postGrazing}
          preGrazing={this.state.preGrazing}
          showTarget={this.state.showTarget}
        />
        <div className="container">
          <p>
            <input type="checkbox" onChange={this.updateShowTarget} />
            Add Pre-grazing target of
            <input
              type="number"
              step="100"
              value={this.state.preGrazing}
              onChange={this.updatePre}
            />
            kg/DM
          </p>
          <p>
            and Post-grazing target of{" "}
            <input
              type="number"
              step="100"
              value={this.state.postGrazing}
              onChange={this.updatePost}
            />{" "}
            kg/DM
          </p>
        </div>
      </div>
    );
  }
}
