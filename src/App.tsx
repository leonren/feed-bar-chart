import React from "react";
import { Layout } from "antd";
import "./App.css";
import InteractiveForm from "./components/InteractiveForm";

const { Content } = Layout;
class App extends React.Component {
  render() {
    const params: any = this.props;
    return (
      <div>
        <Layout className="layout">
          <Content style={{ padding: "0 50px" }}>
            <InteractiveForm />
          </Content>
        </Layout>
      </div>
    );
  }
}

export default App;
