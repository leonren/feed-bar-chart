# Create a responsive bar chart
## Requirement
1. Use typescript
2. Use Material Design
3. Should implement all the functions in this rough sample, including slider bar chart, tooltips, and add a line when you check the checkbox
4. Should use react as this project.
5. I need source code, no need to host.
## Environment Setup

1.  Yarn install

## Deployment

1.  Yarn webpack  
   Build code and generate source file
2.  Yarn plan  
   Verify terraform configuration
3.  Yarn deploy  
    Create resources, identities etc.

## Destroy

1.  Yarn destroy  
    Delete resources, identities etc.

