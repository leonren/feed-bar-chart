module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  moduleFileExtensions: ["js", "json", "ts"],
  collectCoverageFrom: ["./src/**/*.{js,ts}", "!./src/db/**/*"],
  silent: false,
  verbose: true
};
